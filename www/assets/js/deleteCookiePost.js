function deleteCookiePost(successCallback, workingFNCallback)
{
    sendAjaxRequest
    (
        "GET",
        "app_cookie_delete/deleteCookiePost",
        "",
        successCallback,
        "",
        workingFNCallback
    );
}

function deleteCookiePostCallback( result_found, data )
{
  console.log("cookie post deleted");
}