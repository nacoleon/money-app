function validateUserAccount( plaff, plaffer, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_user_crud_validate_account/validate_account",
        "plaff="+plaff+"&plaffer="+plaffer,
        successCallback,
        failCallback,
        workingFNCallback
    );
}


function validateUserAccountOk( result_found, data )
{
   window.location= PROJECT_URL;
}

function validateUserAccountFailed( )
{
   alert('Error de validación de cuenta de usuario.');
}