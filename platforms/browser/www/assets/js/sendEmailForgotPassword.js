

function sendEmailForgotPassword( mail, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_send_email_forgotpassword/getUserId",
        "mail="+mail,
        successCallback,
        failCallback,
        workingFNCallback
    );
}